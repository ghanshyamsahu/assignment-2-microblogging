# Microblogging site
```
In this project you can create a post ,like a post,search posts based on category and user ,and you can see the user statistics.
```
## Getting Started
Cloning a  repository
    to clone the repository in your local system run 
    ```
    git clone https://ghanshyamsahu@bitbucket.org/ghanshyamsahu/assignment-2-microblogging.git
    ```


### Prerequisites


- Djnago 1.8
- postgres 
- psycopg2 
- Homebrew



### Installing


```
install Homebrew 
    - /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)
install postgres
    - brew install postgresql
install Django 1.8  
    - pip install django==1.8
```


## Running the tests
1. run the command - python manage.py runserver 
2. go to adress http://127.0.0.1:8000/admin/ to access admin
    - username- ghanshyamsahu
    - password 0000
3. Models in project-
    - Topic
        - topic_name
    - User
        - username
        - topics
    - Post
        - heading_name
        - post
        - created_on
        - topic
        - user
    - Like
        - username
        - post_id
        - liked_on

4. to create a post  hit the POST Request at  ```http://127.0.0.1:8000/blog/createpost/ ```with details in post body
5. to Fetch  post based on topics  hit the GET Request at ``` http://127.0.0.1:8000/blog/post/?topic=(topic id ) ```with topic id 
6. to Fetch  post created and liked by  perticular  user  hit the GET Request at ``` http://127.0.0.1:8000/blog/post/?user= (username)```
7. to Fatech topics selected  by a user  hit the GET request at ```http://127.0.0.1:8000/blog/topic/?user=(username)```
8. to like any post  hit the POST request at ```http://127.0.0.1:8000/blog/like/ ```with username and post_id in body of request
9. to get the most active user hit the GET request at ```http://127.0.0.1:8000/blog/activity/?queryby=user ```
10. to get the most popular post hit the GET request  at ```http://127.0.0.1:8000/blog/activity/?queryby=post ```


