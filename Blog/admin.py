from django.contrib import admin

# Register your models here.
from Blog.models import Topic,User,Post,Like

admin.site.register(Topic)
admin.site.register(User)
admin.site.register(Post)
admin.site.register(Like)