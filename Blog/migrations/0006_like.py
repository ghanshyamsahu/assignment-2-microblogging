# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Blog', '0005_auto_20180713_0427'),
    ]

    operations = [
        migrations.CreateModel(
            name='Like',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('liked_on', models.DateTimeField(auto_now_add=True)),
                ('post_id', models.ForeignKey(to='Blog.Post')),
                ('user_name', models.ForeignKey(to='Blog.User')),
            ],
        ),
    ]
