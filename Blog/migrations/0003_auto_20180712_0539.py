# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Blog', '0002_auto_20180712_0505'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='topics',
        ),
        migrations.AddField(
            model_name='user',
            name='topics',
            field=models.ManyToManyField(to='Blog.Topic'),
        ),
    ]
