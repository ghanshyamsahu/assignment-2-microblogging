# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Blog', '0003_auto_20180712_0539'),
    ]

    operations = [
        migrations.CreateModel(
            name='Like',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='post',
            name='updated_on',
        ),
        migrations.AddField(
            model_name='like',
            name='post_id',
            field=models.ForeignKey(to='Blog.Post'),
        ),
        migrations.AddField(
            model_name='like',
            name='user_name',
            field=models.ForeignKey(to='Blog.User'),
        ),
    ]
