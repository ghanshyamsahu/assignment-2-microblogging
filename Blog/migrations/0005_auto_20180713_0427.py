# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Blog', '0004_auto_20180712_1306'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='like',
            name='post_id',
        ),
        migrations.RemoveField(
            model_name='like',
            name='user_name',
        ),
        migrations.DeleteModel(
            name='Like',
        ),
    ]
