from django.db import models



class Topic(models.Model):
    topic_name=models.CharField(max_length=200)

    def __unicode__(self):
        return str(self.topic_name)



class User(models.Model):

    username=models.CharField(max_length=200,primary_key=True)
    topics=models.ManyToManyField(Topic)

    def __unicode__(self):
        return str(self.username)


class Post(models.Model):
    heading_name=models.CharField(max_length=200)
    post=models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    topic = models.ForeignKey(Topic,on_delete=models.CASCADE)
    user=models.ForeignKey(User,on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.heading_name)

class Like(models.Model):
    user_name=models.ForeignKey(User,on_delete=models.CASCADE)
    post_id=models.ForeignKey(Post,on_delete=models.CASCADE)
    liked_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.user_name)+'__'+str(self.post_id)


