from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import Get_post,Get_topic,Create_Post,Add_likes,Activity


urlpatterns = [
	url(r'^post/', csrf_exempt(Get_post.as_view()),name="blog-post"),
    url(r'^topic/', csrf_exempt(Get_topic.as_view()),name="blog-topic"),
    url(r'^topic/', csrf_exempt(Get_topic.as_view()),name="blog-topic"),
    url(r'^createpost/', csrf_exempt(Create_Post.as_view()),name="blog-createpost"),
    url(r'^like/', csrf_exempt(Add_likes.as_view()),name="blog-like"),
    url(r'^activity/', csrf_exempt(Activity.as_view()),name="blog-activity"),


]