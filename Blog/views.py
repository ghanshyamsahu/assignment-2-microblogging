
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.http import QueryDict
from django.views.generic import View
from django.http import HttpResponse
from .models import Topic,Post,User,Like
from django.db import IntegrityError


# Class to get Post of user
class Get_post(View):

    # Functions to get post Based on topics or User
    def get(self, request):

        #to Get the all post based on topics

        if 'topic' in request.GET:
            try:
                id=request.GET['topic']
                #creating the post class object

                post_list=Post.objects.filter(topic=id)
                if not post_list:
                    raise ValueError
                result = []
                for post in post_list:
                    tmp = {}
                    tmp['id']=post.id
                    tmp['heading_name']=post.heading_name
                    tmp['post'] = post.post
                    tmp['created on'] = post.created_on
                    tmp['topic'] = post.topic.topic_name
                    tmp['user'] = post.user.username
                    result.append(tmp)
                return HttpResponse(result,status=200)
            except ObjectDoesNotExist as e:
                return HttpResponse(e, status=200)
            except MultiValueDictKeyError as e:
                return HttpResponse(e, status=500)
            except IntegrityError as e:
                return HttpResponse(e, status=500)
            except ValueError:
                return HttpResponse("Post on This  Does Not Exist", status=500)

        # to get the all posts based on usename
        elif 'user' in request.GET:
            try:
                id = request.GET['user']

                #to get all the post created by user
                post_list = Post.objects.filter(user=id)
                if not post_list:
                    raise ValueError
                result = []
                for post in post_list:
                    tmp = {}
                    tmp['id'] = post.id
                    tmp['heading_name'] = post.heading_name
                    tmp['post'] = post.post
                    tmp['created on'] = post.created_on
                    tmp['topic'] = post.topic.topic_name
                    tmp['user'] = post.user.username
                    result.append(tmp)
                #to get all the post liked by user
                like_obj = Like.objects.filter(user_name=id)
                result2 = []
                for i in like_obj:
                    tmp = {}
                    tmp['id']=i.post_id.id
                    tmp['post'] = i.post_id.heading_name
                    result2.append(tmp)

                return HttpResponse("posted by {}<br>{}<br><br><br>liked by {}<br>{}".format(id,result,id,result2), status=200)
            except ObjectDoesNotExist as e:
                return HttpResponse(e, status=200)
            except MultiValueDictKeyError as e:
                return HttpResponse(e, status=500)
            except IntegrityError as e:
                return HttpResponse(e, status=500)
            except ValueError:
                return HttpResponse("{} Does Not Posted Yet".format(id), status=500)

        else :
            return HttpResponse("invalid Parametrs", status=500)


# Class to create new post
class Create_Post(View):
    def post(self, request):
        try:
            post_req = request.POST
            heading_name = post_req['heading_name']
            post = post_req['post']
            topic = post_req['topic']
            user = post_req['user']
            user_obj=User.objects.get(username=user)
            topic_obj=Topic.objects.get(topic_name=topic)

            post_obj=Post(heading_name=heading_name,post=post,topic=topic_obj,user=user_obj)
            post_obj.save()
            return HttpResponse("{} your post {}is sucessfully posted".format(user,heading_name), status=200)
        except ObjectDoesNotExist as e:
            return HttpResponse(e, status=200)
        except MultiValueDictKeyError as e:
            return HttpResponse(e, status=500)
        except IntegrityError as e:
            return HttpResponse(e, status=500)


# Class to show topics selected by perticular user
class Get_topic(View):
    def get(self, request):
        try:
            user = request.GET['user']
            user_obj = User.objects.get(username=user)
            return HttpResponse(user_obj.topics.all(), status=200)
        except Exception as e:
            return HttpResponse(e, status=500)

#class to like post
class Add_likes(View):
    def post(self,request):
        try:
            post_req = request.POST
            user = post_req['username']
            post_id = post_req['post id']
            user_obj = User.objects.get(username=user)
            post_obj = Post.objects.get(id=post_id)

            like_obj = Like(user_name=user_obj, post_id=post_obj)
            like_obj.save()
            return HttpResponse(" {} liked post{} ".format(user,post_id), status=200)

        except ObjectDoesNotExist as e:
            return HttpResponse(e, status=200)

        except MultiValueDictKeyError as e :
            return HttpResponse(e, status=500)

        except IntegrityError as e:
            return HttpResponse(e, status=500)


#class to show User activity
class Activity(View):
    #function to show user activity
    def get(self,request):
        q=request.GET['queryby']

        #if query is done by user it show the most active user
        if q=='user':
            try:
                like_obj=Like.objects.values('user_name').distinct()
                res = {}
                for i in like_obj:
                    res[i['user_name']]=Like.objects.filter(user_name=i['user_name']).count()
                m=0
                k=0
                v=0
                for i,j in res.items():
                    if j>m:
                        k=i
                        v=j
                        m=j
                    else:
                        pass
                return HttpResponse('Total (username , number of post liked)<br><br>{} <br><br>  {} has done most ({} )like'.format(res.items(),k,v))
            except:
                return HttpResponse("error")
        #if query is done by user ,it will show the most active user
        elif q =='post':
            try:
                like_obj=Like.objects.values('post_id').distinct()
                res = {}
                for i in like_obj:
                    res[i['post_id']]=Like.objects.filter(post_id=i['post_id']).count()
                m=0
                k=0
                v=0
                for i,j in res.items():
                    if j>m:
                        k=i
                        v=j
                        m=j
                    else:
                        pass
                return HttpResponse('Total (topic id , number of likes)={} <br><br>post id {}has most likes({} times)'.format(res.items(),k,v))
            except:
                return HttpResponse("error")











